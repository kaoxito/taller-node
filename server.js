//version inicial

var express = require('express'),
  app = express(),
  port = process.env.PORT || 3000;

var path = require('path');

app.listen(port);

console.log('todo list RESTful API server started on: ' + port);

app.get("/",function(req,res){
  //res.send("Hola mundo GET");
  res.sendFile(path.join(__dirname, 'index.html'));
})

app.post("/",function(req,res){
  res.send("Soy post");
})

app.get("/clientes/:idcliente", function(req, res) {
  var clientes = ["Tony Ramos","Lisa","Michael","Ginger","Food"];
  res.json(clientes);
  //res.send("Cliente #:"+req.params.idcliente)
})

app.put("/clientes/", function(req, res){
  res.json("Petición PUT");
})

app.delete("/clientes/", function(req, res){
  res.json("Petición DELETE");
})

// app.get("/v1/movimientos", function(req,res){
//   res.sendFile(path.join(__dirname, 'movimientosv1.json'));
// })
app.get("/v1/movimientos", (req,res) => res.sendFile(path.join(__dirname, 'movimientosv1.json')));

var movimientosJSON = require('./movimientosv2.json');

// app.get('/v2/movimientos', function(req, res){
//   res.json(movimientosJSON);
// })
app.get('/v2/movimientos', (req, res) => res.json(movimientosJSON));

app.get('/v2/movimientos/:id', (req, res) => res.send(movimientosJSON[req.params.id-1]));

// /v2/movimientosq?id=22&otrovalor=hola
app.get('/v2/movimientosq', function(req, res){
  console.log(req.query);
  res.send("recibido");
});

let bodyparser = require('body-parser');

app.use(bodyparser.json());

app.post('/v2/movimientos', function(req, res){
  let nuevo = req.body;
  nuevo.id = movimientosJSON.length + 1;
  movimientosJSON.push(nuevo);
  res.send("Movimientos dado de alta");
  console.log(movimientosJSON[100]);
})